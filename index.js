const url = require('url');
const { Curl, CurlFeature } = require('node-libcurl');
const { JSDOM } = require('jsdom');
const Discord = require('discord.js');

const token = require('./token.json');

const client = new Discord.Client({intents: [Discord.Intents.FLAGS.GUILDS, Discord.Intents.FLAGS.GUILD_MESSAGES]});

client.on('ready', () => {
    console.log('Logged in as', client.user.username);
});

const regx = /(?:(?:https?|http):\/\/|www\.|m\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/igm;

client.on('message', message => {
    if(message.author == client.user) return;
    const urls = message.content.match(regx);
    if(urls == null) return;
    if(urls.length < 1) return;
    let firstUrl;
    try{
        firstUrl = new url.URL(urls[0]);
    }catch(err){
        return;
    }
    
    if(firstUrl.host !== 'instagram.com' && firstUrl.host !== 'www.instagram.com') return;
    process(firstUrl.href, message);
});

function request(url){
    return new Promise((resolve, reject)=>{
    const curl = new Curl();
    curl.setOpt(Curl.option.HTTPHEADER, ['User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0']);
    curl.setOpt('URL', url);
    curl.setOpt('FOLLOWLOCATION', true);
    curl.enable(CurlFeature.NoDataParsing);
    curl.on('end', async function (statusCode, data, headers) {
        resolve(data);
        this.close();
    });
    curl.on('error', ()=>{
        curl.close.bind(curl);
        reject();
    });
    curl.perform();
    });
}

async function process(url, message){
    const html = await request(url);
    const searchPage = new JSDOM(html).window.document;
    const metas = searchPage.getElementsByTagName('meta');
    var foundVideo;
    var foundImage;
    for(meta of metas){
        const property = meta.getAttribute('property');
        if(property == null) continue;
        if(!property.startsWith('og:')) continue;
        if(property == 'og:video') foundVideo = meta.getAttribute('content');
        if(property == 'og:image') foundImage = meta.getAttribute('content');
    }
    if(foundVideo != undefined){
        const video = await request(foundVideo);
        if(video.length > 8388608){
            message.reply(`${foundVideo}`);
        }else{
            message.reply('Media', {files: [{attachment: video, name: 'instagram.mp4'}]});
        }
    }else if(foundImage != undefined){
        const image = await request(foundImage);
        if(image.length > 8388608){
            message.reply(`${foundImage}`);
        }else{
            message.reply('Media', {files: [image]});
        }
    }else{
        message.reply(`Unable to extract media.`);
    }
}

client.login(token);
